var app_directions = extend(
	{
		model: "direction",
		url: "catalogos/directions",
		combos: {
			estado_id: app.combos.estados,
			pais_id: app.combos.paises
		},
		afterSave: function() {
			app_directions.tools.unsetOptions();
		},
		view: {
			id: "main_directions",
			rows: [
				{
					cells: [
						{
							id: "win_directions",
							rows: [
								extend(
									{
										css: "own_header",
										cols: [
											{ view: "label", label: "Sucursales" },
											extend(
												{
													label: "Nuevo",
													click: function() {
														app_directions.new();
													}
												},
												"sbutton"
											)
										]
									},
									"toolbar"
								),
								extend(
									{
										id: "dt_directions",
										columns: [
											{
												id: "sucursal",
												header: ["Sucursal", serverFilter()],
												fillspace: true
											},
											{
												id: "encargado",
												header: ["encargado", serverFilter()],
												fillspace: true
											},
											{
												id: "direccion",
												header: ["direccion", serverFilter()],
												fillspace: true
											}
										],
										on: {
											onItemClick: function(id) {
												var item = this.getItem(id);
												app_directions.tools.editId = item.id;
											},
											onItemDblClick: function(id) {
												var item = this.getItem(id);
												app_directions.record(item.id);
												app_directions.tools.setOption("record");
											}
										}
									},
									"datatable"
								)
							]
						},
						{
							id: "win_direction",
							rows: [
								extend(
									{
										css: "own_header",
										cols: [
											{ view: "label", label: "Sucursales" },
											extend(
												{
													click: function() {
														app_directions.cancel();
														app_directions.close_e();
														app_directions.tools.unsetOptions();
													}
												},
												"dbutton"
											),
											extend(
												{
													click: function() {
														var data = { evento: {}, detalle: [] };
														data = $$("frm_direction").getValues();
														if ($$("frm_direction").validate()) {
															app.ajax(
																"catalogos/directions/save",
																data,
																function(response) {
																	if (response.success) {
																		if (
																			$$("dt_directions").getItem(
																				response.id
																			) == undefined
																		) {
																			$$("dt_directions").add(response.data);
																		} else {
																			$$("dt_directions").updateItem(
																				response.id,
																				response.data
																			);
																		}

																		app_directions.cancel();
																		app_directions.close_e();
																		app_directions.tools.unsetOptions();
																	} else {
																		if (app_directions.tools.editId == 0)
																			app_directions.tools.setOption("new");
																		else
																			app_directions.tools.setOption("record");
																	}
																}
															);
														}
													}
												},
												"sbutton"
											)
										]
									},
									"toolbar"
								),
								extend(
									{
										id: "frm_direction",
										elements: [
											{
												cols: [
													extend(
														{
															label: "Nombre del Encargado",
															name: "encargado",
															attributes: { maxlength: 50 }
														},
														"rtext"
													),
													extend(
														{
															label: "Nombre de sucursal",
															name: "sucursal",
															attributes: { maxlength: 32 }
														},
														"rtext"
													),
													extend(
														{
															label: "Teléfono",
															name: "telefono",
															id: "telefono",
															attributes: { maxlength: 11 }
														},
														"rtext"
													),
													extend(
														{
															label: "Dirección",
															name: "direccion",
															id: "direccion",
															attributes: { maxlength: 50 }
														},
														"rtext"
													),
													extend(
														{
															label: "País",
															name: "pais_id",
															width: 150,
															id: "cliente_pais_id",
															on: {
																onChange: function(val) {
																	$$("cliente_estado_id").show();
																	$$("cliente_estado").hide();
																	if (val != undefined) {
																		if (val != 1) {
																			$$("cliente_estado_id").hide();
																			$$("cliente_estado").show();
																		}
																	}
																}
															}
														},
														"rcombo"
													),
													extend(
														{
															label: "Estado",
															name: "estado_id",
															id: "cliente_estado_id",
															width: 150
														},
														"combo"
													)
												]
											}
										],
										rules: {
											encargado: required,
											sucursal: required,
											telefono: required,
											direccion: required
										}
									},
									"form"
								)
							]
						}
					]
				}
			]
		}
	},
	app_std
);
webix.ready(function() {
	webix.ui(
		extend(
			{
				id: "win_s_p",
				head: "Selección de productos y cargos",
				body: {
					rows: [
						extend(
							{
								id: "direction_s_p",
								yCount: 10,
								columns: [
									{
										id: "producto",
										header: ["Producto", serverFilter()],
										fillspace: true
									},
									{ id: "tipo", header: ["Tipo", serverSelectFilter()] },
									{ id: "precio", header: "Precio" }
								],
								on: {
									onItemDblClick: function(id) {
										var item = webix.copy(this.getItem(id));
										var productos = $$("dt_direction_detalle").serialize();
										var i = 0,
											exists = false;
										for (i = 0; i < productos.length; i++) {
											if (
												productos[i].tipo_c == item.tipo_c &&
												productos[i].clave == item.clave
											)
												exists = true;
										}
										if (!exists) {
											item.cantidad = 1;
											item.subtotal = item.cantidad * item.precio;
											item.cargo = item.tipo_c;
											item.producto_id = item.clave;
											$$("dt_direction_detalle").add(item);
											$$("win_s_p").hide();
										} else
											webix.message(
												"El producto seleccionado ya existe en la lista actual"
											);
									}
								}
							},
							"datatable"
						),
						{
							cols: [
								{ template: "Doble click para seleccionar", height: 25 },
								extend(
									{
										label: "Cerrar",
										click: function() {
											$$("win_s_p").hide();
										}
									},
									"button"
								)
							]
						}
					]
				}
			},
			"window"
		)
	);
});
