<?php
class Directions extends MY_Controller
{
	public $model = 'direction';
	public function getDirections() {
        $this->json($this->direction->directions());
	}
    public function detalle($id, $tipo = "")
    {
        $data = $this->direction->detalle($id, $tipo);

        $this->json($data);
    }
    public function save()
    {
        // if (!isset($_POST['id'])) {
        //     $_POST['usuario_id'] = $this->user_id();
        // }
        parent::save();
    }
}
